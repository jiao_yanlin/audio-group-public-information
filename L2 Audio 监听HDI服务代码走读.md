## L2 Audio HDI服务监听机制

### 介绍

在L2 标准系统中，常驻服务主要有HDI service、Framework service两类；通常情况下，HDI service早于Framework service被拉起，但是依赖时序是不可靠的，况且可能存在有的HDI服务是有条件拉起的。

依赖HDI service的Framework service需要监听HDI service，监听到其上线方可连接上HDI service



### 代码走读

服务会实现监听器相关接口(虚函数)

services\src\audio_policy\server\service\include\audio_policy_service.h

```c++
class AudioPolicyService : public IPortObserver, public IDeviceStatusObserver {
public:	
    void OnDeviceStatusUpdated(DeviceType deviceType, bool connected, void *privData);
    void OnServiceConnected();
    //......
    AudioPolicyService()
        : mAudioPolicyManager(AudioPolicyManagerFactory::GetAudioPolicyManager()),
          mConfigParser(ParserFactory::GetInstance().CreateParser(*this))
    {
          //构造函数中初始化监听器
          mDeviceStatusListener = std::make_unique<DeviceStatusListener>(*this);
    }
	//......
}
```

audio_policy会在构造函数中初始化监听器监听（此机制由HDI框架提供）

在init函数中注册-RegisterDeviceStatusListener

```c++
bool AudioPolicyService::Init(void)
{
    mAudioPolicyManager.Init();
    if (!mConfigParser.LoadConfiguration()) {
        MEDIA_ERR_LOG("Audio Config Load Configuration failed");
        return false;
    }
    if (!mConfigParser.Parse()) {
        MEDIA_ERR_LOG("Audio Config Parse failed");
        return false;
    }

    std::unique_ptr<AudioFocusParser> audioFocusParser;
    audioFocusParser = make_unique<AudioFocusParser>();
    std::string AUDIO_FOCUS_CONFIG_FILE = "/etc/audio/audio_interrupt_policy_config.xml";

    if (audioFocusParser->LoadConfig(focusTable_[0][0])) {
        MEDIA_ERR_LOG("Audio Interrupt Load Configuration failed");
        return false;
    }
    //监听器注册
    if (mDeviceStatusListener->RegisterDeviceStatusListener(nullptr)) {
        MEDIA_ERR_LOG("[Policy Service] Register for device status events failed");
        return false;
    }

    return true;
}
```

再来看监听器的实现

services\src\audio_policy\server\service\src\listener\device_status_listener.cpp

```c++
int32_t DeviceStatusListener::RegisterDeviceStatusListener(void *privData)
{
    hdiServiceManager_ = HDIServiceManagerGet();
    if (hdiServiceManager_ == nullptr) {
        MEDIA_ERR_LOG("[DeviceStatusListener]: Get HDI service manager failed");
        return ERR_OPERATION_FAILED;
    }

    privData_ = privData;
    listener_ = HdiServiceStatusListenerNewInstance();
    listener_->callback = OnServiceStatusReceived;
    listener_->priv = (void *)this;
    int32_t status = hdiServiceManager_
    ->RegisterServiceStatusListener(hdiServiceManager_, listener_,
                                    DeviceClass::DEVICE_CLASS_AUDIO);
    if (status != HDF_SUCCESS) {
        MEDIA_ERR_LOG("[DeviceStatusListener]: Register service status listener failed");
        return ERR_OPERATION_FAILED;
    }

    return SUCCESS;
}
```

hdiServiceManager_是监听HDI服务的关键，此处传入了 OnServiceStatusReceived ，即监听HDI服务状态的回调

其实现代码

```c++
static void OnServiceStatusReceived(struct ServiceStatusListener *listener,
                                    struct ServiceStatus *serviceStatus)
{
    MEDIA_DEBUG_LOG("[DeviceStatusListener] OnServiceStatusReceived in");
    MEDIA_DEBUG_LOG("[DeviceStatusListener]: service name: %{public}s", serviceStatus->serviceName);
	//此处会监听AUDIO_HDI_SERVICE_NAME，deviceObserver_就是audio_policy，此时会通知audio_policy 
    //可以连接audio hdi service
    if (!AUDIO_HDI_SERVICE_NAME.compare(std::string(serviceStatus->serviceName))) {
        if (serviceStatus->status == SERVIE_STATUS_START) {
            DeviceStatusListener *deviceStatusListener = reinterpret_cast<DeviceStatusListener *>(listener->priv);
            deviceStatusListener->deviceObserver_.OnServiceConnected();
        }
    }

    DeviceType deviceType = GetDeviceTypeByName(serviceStatus->info);
    if (deviceType != DEVICE_TYPE_INVALID) {
        if ((serviceStatus->status == SERVIE_STATUS_START) || (serviceStatus->status == SERVIE_STATUS_STOP)) {
            bool connected = (serviceStatus->status == SERVIE_STATUS_START) ? true : false;
            MEDIA_DEBUG_LOG("[DeviceStatusListener]: Device: %{public}s: connected: %{public}s",
                            serviceStatus->info, connected ? "true" : "false");
            DeviceStatusListener *deviceStatusListener = reinterpret_cast<DeviceStatusListener *>(listener->priv);
            deviceStatusListener->deviceObserver_.OnDeviceStatusUpdated(deviceType, connected,
                                                                        deviceStatusListener->privData_);
        }
    }
}
```

