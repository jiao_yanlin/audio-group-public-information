# OpenHarmony 标准系统 IPC通信机制

### 介绍

 IPC（Inter-Process Communication）机制用于实现跨进程通信，使用Binder驱动，用于设备内的跨进程通信。通常采用客户端-服务器（Client-Server）模型，服务请求方（Client）可获取提供服务提供方（Server）的代理 （Proxy），并通过此代理读写数据来实现进程间的数据通信。通常，系统能力（System Ability）Server侧会先注册到系统能力管理者（System Ability Manager，缩写SAMgr）中，SAMgr负责管理这些SA并向Client提供相关的接口。Client要和某个具体的SA通信，必须先从SAMgr中获取该SA的代理，然后使用代理和SA通信。三方应用可以使用FA提供的接口绑定服务提供方的Ability，获取代理，进行通信。下文使用Proxy表示服务请求方，Stub表示服务提供方。 

 ![img](https://gitee.com/openharmony/communication_ipc/raw/master/figures/ipc-architecture.png) 

*详细可参考 ipc仓:* 

 [communication_ipc: Inter-process communication (IPC) and Remote Procedure Call (RPC) | 跨进程通信与跨设备的远程过程调用 (gitee.com)](https://gitee.com/openharmony/communication_ipc) 



### 代码走读

**Native侧实现跨进程通信的基本步骤：**

1. 定义接口类

   接口类继承IRemoteBroker，定义描述符、业务函数和消息码。

2. 实现服务提供端(Stub)

   Stub继承IRemoteStub(Native)，除了接口类中未实现方法外，还需要实现AsObject方法及OnRemoteRequest方法。

3. 实现服务请求端(Proxy)

   Proxy继承IRemoteProxy(Native)，封装业务函数，调用SendRequest将请求发送到Stub。

4. 注册SA

   服务提供方所在进程启动后，申请SA的唯一标识，将Stub注册到SAMgr。

5. 获取SA

6. 通过SA的标识和设备标识，从SAMgr获取Proxy，通过Proxy实现与Stub的跨进程通信。

   

接下来以 audio_standard 的 audio_policy为例，其

#### 目录结构

大致可以分为client、server两块

1、定义接口类

class IAudioPolicy : public IRemoteBroker

路径：services\include\audio_policy\client\audio_policy_base.h

3、实现服务请求端

class AudioPolicyProxy : public IRemoteProxy<IAudioPolicy>

路径：services\include\audio_policy\client\audio_policy_proxy.h

2、实现服务提供端

class AudioPolicyManagerStub : public IRemoteStub<IAudioPolicy>

路径：services\include\audio_policy\client\audio_policy_manager_stub.h

4、注册SA

5、获取SA

6、从SA获取proxy，通过proxy实现与stub通信



#### 代码实际调用过程

client端调用

```c++
int32_t AudioPolicyProxy::SetStreamVolume(AudioStreamType streamType, float volume)
{
    MessageParcel data;
    MessageParcel reply;
    MessageOption option;
    //token用于鉴权
    if (!data.WriteInterfaceToken(GetDescriptor())) {
        MEDIA_ERR_LOG("AudioPolicyProxy: WriteInterfaceToken failed");
        return -1;
    }
	//data携带入参
    data.WriteInt32(static_cast<int32_t>(streamType));
    data.WriteFloat(volume);
    int32_t error = Remote()->SendRequest(SET_STREAM_VOLUME, data, reply, option);
    if (error != ERR_NONE) {
        MEDIA_ERR_LOG("set volume failed, error: %d", error);
        return error;
    }
    //reply携带返回值
    return reply.ReadInt32();
}
```

stub端接收

```c++
int AudioPolicyManagerStub::OnRemoteRequest(
    uint32_t code, MessageParcel &data, MessageParcel &reply, MessageOption &option)
{
    //校验token
    if (data.ReadInterfaceToken() != GetDescriptor()) {
        return -1;
    }
    switch (code) {
        case SET_STREAM_VOLUME:
            SetStreamVolumeInternal(data, reply);
            break;
        // ......
    }
}

void AudioPolicyManagerStub::SetStreamVolumeInternal(MessageParcel &data, MessageParcel &reply)
{
    AudioStreamType streamType = static_cast<AudioStreamType>(data.ReadInt32());
    //从data取出入参
    float volume = data.ReadFloat();
    int result = SetStreamVolume(streamType, volume);
    //给reply写入返回值
    if (result == SUCCESS)
        reply.WriteInt32(MEDIA_OK);
    else
        reply.WriteInt32(MEDIA_ERR);
	
}
```

#### 更多

因为框架已经搭建完毕，作为后来者，按部就班的增加新的接口即可，更多资料 ->

*ipc仓:* 

 [communication_ipc: Inter-process communication (IPC) and Remote Procedure Call (RPC) | 跨进程通信与跨设备的远程过程调用 (gitee.com)](https://gitee.com/openharmony/communication_ipc) 