## PulseAudio Log开关

配置文件路径：/system/etc/pulse/daemon.conf

```c++
//取出配置文件
hdc std file recv /system/etc/pulse/daemon.conf
```



```c++
//修改配置文件,开启log
log-target = /data/data/pa_log.txt
log-level = debug
log-meta = yes
log-times = yes
log-backtrace = 0
```



```c++
//推入配置文件
hdc std file send ./daemon.conf /system/etc/pulse/
```



**Tips: 当前只能打印 PulseAudio 服务端进程日志；无法打印客户端进程日志**

## 代码跳转技巧
在查看本地代码的时候，把 hdi、pulse audio的工程文件放到audio_standard的工程根目录下面
这样就可以查看hdi、pulse audio接口注释和实现了
hdi仓：openHarmony\drivers\peripheral
pulse audio仓：openHarmony\third_party\pulseaudio

