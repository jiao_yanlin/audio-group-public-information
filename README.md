# 音频组公共资料

#### 介绍
该仓库为音频组在蓝区的公告资料库

![](https://gitee.com/yangshuai0222/audio-group-public-information/raw/master/%E7%BB%98%E5%9B%BE1.png)

### wiki分类
#### 环境配置
OpenHarmony代码下载、开发准备教程
https://gitee.com/yangshuai0222/audio-group-public-information/commit/a095415a74ecef302aa711e5e9f15e29a8b2c01f

L1编译相关
https://gitee.com/yangshuai0222/audio-group-public-information/commit/4d8c36139953b8b4e4508eb3e59138eab26da72a

L1烧写教程
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/L1%203516%20%E7%83%A7%E5%86%99%E6%95%99%E7%A8%8B.md

L1 入门
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/%E8%BD%BB%E9%87%8F%E5%92%8C%E5%B0%8F%E5%9E%8B%E7%B3%BB%E7%BB%9F%E5%85%A5%E9%97%A8

NFS测试步骤
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/NFS%E4%BB%A3%E7%A0%81%E6%B5%8B%E8%AF%95%E6%AD%A5%E9%AA%A4

L2 RK3568烧写
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/L2%20rk3568%20%E7%83%A7%E5%86%99%E6%95%99%E7%A8%8B.md

L2 编译命令
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/L2%20%E7%BC%96%E8%AF%91%E5%91%BD%E4%BB%A4

#### 常用命令
HDC命令
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/HDC%E5%91%BD%E4%BB%A4%E6%8C%87%E5%8D%97

git fork、代码上库流程
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/%E4%B8%8B%E8%BD%BD%E5%8D%95%E4%BB%93%E4%BB%A3%E7%A0%81%E5%8F%8A%E4%B8%8A%E5%BA%93%E8%AE%B0%E5%BD%95.docx

#### 人力阵型
人力沟通地图
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/%E4%BA%BA%E5%8A%9B%E6%B2%9F%E9%80%9A%E5%9C%B0%E5%9B%BE

#### 代码走读
根据现有录音demo进行录音代码走读.docx
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/%E6%A0%B9%E6%8D%AE%E7%8E%B0%E6%9C%89%E5%BD%95%E9%9F%B3demo%E8%BF%9B%E8%A1%8C%E5%BD%95%E9%9F%B3%E4%BB%A3%E7%A0%81%E8%B5%B0%E8%AF%BB.docx

ipc通信机制介绍
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/OH%20IPC%E9%80%9A%E4%BF%A1%E6%9C%BA%E5%88%B6%E4%BB%8B%E7%BB%8D-%E4%BB%A5audio%E4%B8%BA%E4%BE%8B.md

AudioPolicy服务初始化
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/L2%20audio_standard%20%E6%9C%8D%E5%8A%A1%E5%90%AF%E5%8A%A8%E6%B5%81%E7%A8%8B%E8%B5%B0%E8%AF%BB.md

AudioPolicy监听HDI服务上线
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/L2%20Audio%20%E7%9B%91%E5%90%ACHDI%E6%9C%8D%E5%8A%A1%E4%BB%A3%E7%A0%81%E8%B5%B0%E8%AF%BB.md

#### 技术文档
opensles
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/OpenSL_ES_Specification_1.0.1.pdf

#### pulseAudio相关

pulseAudio文档
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/PulseAudio%20under%20the%20hood.pdf

pulseAudio流缓冲区属性分析
待补充

pulseAudio 开启 log方法
https://gitee.com/yangshuai0222/audio-group-public-information/blob/master/PulseAudio%20%E5%BC%80%E5%90%AF%20Log%20%E6%89%93%E5%8D%B0.md