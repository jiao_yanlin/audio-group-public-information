## L2 Audio 编译文件一览

### framework

pulse_audio_service_adapter

audio_capturer_source

audio_renderer_sink

opensles

audio_capturer

audio_renderer

audio

### client

audio_client

audio_policy_client

audio_ringtone_client

### services

audio_service

audio_dump

audio_policy_service

### others

sndfile

pulsecommon

pulseaudio

module-native-protocol-fd

module-native-protocol-tcp

module-native-protocol-unix

module-cli-protocol-unix

module-pipe-sink

module-pipe-source

module-suspend-on-idle

module-hdi-sink

module-hdi-source

pulse

pulse-simple

pulse-mainloop-glib

pulsecore

cli

protocol-cli

protocol-native