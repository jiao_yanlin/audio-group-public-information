#### OpenHarmony-环境配置

1. 操作系统与软件安装

        (1) 给蓝区主机重装系统，安装win10操作系统；

        (2) 安装vmwave虚拟机（设置虚拟机内存16G，处理器数量8个，硬盘200G）并加载ubuntu镜像文件；

        (3) 安装vscode(查阅修改代码)、mobaxterm(连接代码环境)、samba(配置映射代码);

        (4) 可选，更新Ubuntu软件源使用[国内的镜像源](https://zhuanlan.zhihu.com/p/61228593)（更新下载软件更快）

2. 代码下载
   
   (1) 根据 [获取源码链接](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/get-code/sourcecode-acquire.md) 中的“获取方式1”来获取代码；
   
   (2) gitee账号注册；
   
   (3) linux上ssh配置，[配置方法链接](https://blog.csdn.net/xiong_xin/article/details/106598940)
   
   (4) 安装git客户端和git-lfs并配置用户信息
   
		``` shell
		git config --global user.name "yourname"
		git config --global user.email "your-email-address"
		git config --global credential.helper store
		``` 
   
   (5) 安装码云repo工具，执行如下命令：
   ``` shell
	curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > /usr/local/bin/repo  #如果没有权限，可下载至其他目录，并将其配置到环境变量中(~/bin下重进连接即可)
   chmod a+x /usr/local/bin/repo
   pip3 install -i https://repo.huaweicloud.com/repository/pypi/simple requests
   ```

        备注：若出现需要安装curl、pip3等工具的情况，安装一下即可

        (6) OpenHarmony主干代码获取，通过repo + ssh下载

        > repo init -u git@gitee.com:openharmony/manifest.git -b master --no-repo-verify
          repo sync -c
          repo forall -c 'git lfs pull'

3. 代码编译
   
   [代码编译参考链接](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-standard-running-rk3568-build.md#https://gitee.com/link?target=https%3A%2F%2Fdocs.docker.com%2Fengine%2Finstall%2F)
   
   (1) docker方式获取编译工具链
   
     A. 执行prebuilts，在源码根目录执行脚本，安装编译器及二进制工具
   
     `bash build/prebuilts_download.sh`
   
     B. 安装docker,[安装方法链接](https://docs.docker.com/engine/install/ubuntu/)
   
     C. 获取docker镜像，执行如下命令：
   
      `docker pull swr.cn-south-1.myhuaweicloud.com/openharmony-docker/openharmony-docker-standard:1.0.0`
   
      D. 进入源码根目录执行如下命令，从而进入docker构建环境
   
      `docker run -it -v $(pwd):/home/openharmony swr.cn-south-1.myhuaweicloud.com/openharmony-docker/openharmony-docker:1.0.0`
   
      E.执行如下命令进行编译（编译成功后显示 rk3568 build success）
   
      `./build.sh --product-name rk3568 --ccache`
   
   (2) 使用安装包方式获取编译工具链
   
      A. 执行prebuilts，在源码根目录执行脚本，安装编译器及二进制工具
   
      `bash build/prebuilts_download.sh`
   
      B. 在终端输入如下命令安装编译相关的依赖工具：
   
       `sudo apt-get update && sudo apt-get install binutils git git-lfs gnupg flex bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip m4 bc gnutls-bin python3.8 python3-pip ruby  liblz4-tool libssl-dev libncurses5`
   
       C. 进入源码根目录，执行如下命令编译（编译成功后显示 rk3568 build success）
   
       `./build.sh --product-name rk3568 --ccache`

4. 编译问题
   
   采用安装包方式获取编译工具链的方式，会出现若干编译问题
   
   (1) 缺少`libtinfo.so.5`问题
   
   安装libncurses5即可解决，执行命令：`sudo apt-get install libncurses5`。[参考链接](https://blog.csdn.net/lsqtzj/article/details/108553576)
   
   (2) No such file or directory: 'java'问题
   
   安装openjdk-8-jde即可解决。[参考链接](https://www.jianshu.com/p/0b6e4ce40535)
   
   (3) /usr/bin/dpkg returned an error code (1) 问题
   
   重新创建/var/lib/dpkg目录下的info目录。[参考链接](https://jingyan.baidu.com/article/647f0115d248ae7f2148a8a3.html)

   (4)openssl/bio.h: No such file or directory 问题
   安装libssl-dev解决： sudo apt-get install libssl-dev