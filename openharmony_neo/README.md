# OpenHarmony_neo

#### 介绍
OpenHarmony开发笔记

# 资料汇总

## 环境配置

- [windows环境准备](https://gitee.com/hangliebe/openharmony_neo/blob/master/wiki/windows环境准备.md)
- [最简洁的编译环境配置](https://gitee.com/hangliebe/openharmony_neo/blob/master/wiki/最简洁的编译环境配置.md)
- [板子烧录教程](https://gitee.com/hangliebe/openharmony_neo/blob/master/wiki/板子烧录教程.md)

## 码云常规操作

- [如何提交PR](https://gitee.com/hangliebe/openharmony_neo/blob/master/wiki/如何提交一个PR.md)
- [如何修改PR内容](https://gitee.com/hangliebe/openharmony_neo/blob/master/wiki/如何修改PR内容.md)

## audio材料

OpenHarmony关键仓库：

[multimedia_audio_standard](https://gitee.com/openharmony/multimedia_audio_standard)

### wiki

[audio框架图和流程图](https://gitee.com/hangliebe/openharmony_neo/blob/master/wiki/audio/audio框架图和流程图.md)

### 音频开发资料汇总

Linux音频编程指南 http://www.ibm.com/developerworks/cn/linux/l-audio/
深入OSS开发 http://www.ibm.com/developerworks/cn/linux/l-ossdev/
OSS–跨平台的音频接口简介 http://www.ibm.com/developerworks/cn/linux/l-ossapi/index.html
OSS官方文档 http://www.opensound.com/developer/index.html

