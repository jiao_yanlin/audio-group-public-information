# 如何提交一个PR

## Folk代码仓

从openharmony找到要修改的代码仓，folk到个人空间。

[下载并查看教学视频。](https://gitee.com/hangliebe/openharmony_neo/raw/master/video/folk_step.mp4)

## 提交一个PR

在个人空间修改代码后，点击`+Pull Request`按钮，提交PR。

[下载并查看教学视频。](https://gitee.com/hangliebe/openharmony_neo/raw/master/video/pull_request_step.mp4)