OpenHarmony的编译最好使用Linux环境，要进行OpenHarmony的开发，如果你只有一台Windows操作系统的PC，没有关系，我们可以通过虚拟机软件创建虚拟的Linux系统。

为了更好的进行开发后续工作，建议按以下步骤给自己的Windows系统做简单环境配置。

## 下载安装git

1 安装 git工具，点击[这里](https://git-scm.com/download/win)可以下载windows版本。

![Git-install](https://gitee.com/hangliebe/openharmony_neo/raw/master/pic/Git-install.gif)

### wget工具

[点击下载](https://eternallybored.org/misc/wget/1.21.3/64/wget.exe)

将`wget.exe`拷贝到`C:\Program Files\Git\mingw64\bin`

### tree工具

[点击下载](http://downloads.sourceforge.net/gnuwin32/tree-1.5.2.2-bin.zip)

安装后在安装目录`D:\Program Files (x86)\GnuWin32\bin`下找到`tree.exe`。

将`tree.exe`拷贝到`C:\Program Files\Git\mingw64\bin`