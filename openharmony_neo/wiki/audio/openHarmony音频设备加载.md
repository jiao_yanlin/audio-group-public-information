OpenHarmony 音频框架代码开源在[multimedia_audio_standard](https://gitee.com/openharmony/multimedia_audio_standard)。

了解下openharmony音频框架的整体架构。

## 音频框架注册监听设备上下线

![设备上下线](https://gitee.com/hangliebe/openharmony_neo/raw/master/pic/audio/%E6%BF%80%E6%B4%BB%E8%AE%BE%E5%A4%87.png)

### CLASS类型和设备类型

设备的[ClassType类型](https://gitee.com/openharmony/multimedia_audio_standard/blob/master/services/include/audio_policy/server/service/common/audio_module_info.h#L53)可以分为`TYPE_PRIMARY，TYPE_A2DP，TYPE_USB，TYPE_INVALID`等几种。而具体的设备类型可以[在这里查看](https://gitee.com/openharmony/multimedia_audio_standard/blob/master/interfaces/inner_api/native/audiocommon/include/audio_info.h#L78)。

注意，ClassType类型实际上对应设备连接方式，而一种连接方式可以有多种设备的连接，一个具体的设备最终可以对应到一个具体的`module`。要加载的设备信息基本都配置在`audio_policy_config.xml`文件中。以rk3568为例，可以[点击查看](https://gitee.com/openharmony/multimedia_audio_standard/blob/master/services/src/audio_policy/server/etc/rk3568/audio_policy_config.xml)其配置。

### 回调说明

回调的[service状态](https://gitee.com/openharmony/drivers_adapter/blob/master/uhdf2/include/hdi/iservstat_listener_hdi.h#L26)主要四种状态，`SERVIE_STATUS_START`和`SERVIE_STATUS_CHANGE`的处理：

**前者对应服务启动，需要加载`TYPE_PRIMARY`类型设备**，这个过程会调用`PulseAudio`模块的`pa_context_load_module`去加载一个[pa_module](https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/Developer/ModuleAPI/), 对于`DEVICE_TYPE_SPEAKER`或者`DEVICE_TYPE_MIC`类型的设备还需要将它激活(AudioPolicyManager::SetDeviceActive)。

**后者对应有服务状态信息有变更**，可以对应新增的设备或者断开的设备处理。

>`pa_module`中的`init`指针指向`pa_init`函数，下面一节会介绍`pa_init`函数做了些什么事情。

### 蓝牙设备上下线回调说明

蓝牙设备注册监听的回调接口（`RegisterObserver`）如下设置，当蓝牙设备上下线时候，`AudioOnConnectionChanged`回调被触发，进一步再调用到`AudioPolicyService::OnDeviceStatusUpdated`，具体[查看文件](https://gitee.com/openharmony/multimedia_audio_standard/blob/master/services/src/audio_bluetooth/client/audio_bluetooth_manager.cpp#L69)。

```
openharmony\multimedia_audio_standard\services\src\audio_bluetooth\client\audio_bluetooth_manager.cpp
void RegisterObserver(IDeviceStatusObserver &observer)
{
    ……
    g_deviceObserver = &observer;
    g_btA2dpSrcObserverCallbacks = new BluetoothA2dpSrcObserver(&g_hdiCallacks);
    g_proxy->RegisterObserver(g_btA2dpSrcObserverCallbacks);
}
```

## 模块初始化

下面一张图是audio fwk模块加载音频设备的时序图，包括source设备（MIC），sink设备（CLASS_TYPE_PRIMARY或者CLASS_TYPE_A2DP类型）。

![加载设备](https://gitee.com/hangliebe/openharmony_neo/raw/master/pic/audio/加载设备.png)