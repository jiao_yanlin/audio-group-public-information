PR提交后需要重新修改，不需要删除之前的PR并重新创建，可以在已有PR上追加修改。具体可以根据下面指导操作

## 1 建立链接

```bash
# git clone 个人folk仓
git clone https://gitee.com/hangliebe/xxx
cd xxx
# 与远端源仓建立链接
git remote add base https：//远端源仓库
```

通过`git remote add` 将本地仓库和远端仓库建立一个链接。`base`是为远端源仓起的名字，后面紧接着远端源仓的地址。

## 2 同步代码

如有冲突就解冲突

```bash
git pull
git pull --rebase base master 
```

## 3 修改并提交

修改代码后进行提交

```bash
git add .
# amend方式提交，这样可以避免产生多次commit记录
git commit --amend
# focus push
git push origin -f
```