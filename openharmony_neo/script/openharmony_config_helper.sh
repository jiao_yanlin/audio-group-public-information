echo "============================================================================"
echo "作者：占航（Hansen）"
echo "主页：https://gitee.com/hangliebe"
echo "温馨提示："
echo "如果你已经配置好了环境，建议不要执行此脚本，直接下载代码编译就可以了。"
echo "该脚本执行耗时较长，并且很多步骤需要开发人员进行交互确认。"
echo "请确保你的ubuntu系统可以正常访问网络。"
echo "请确保你的ubuntu系统有足够的内存（16G）和足够的空间（200G）。"
echo "============================================================================"

read -n1 -p "确认是否要继续Y/N:" GO_ON
case $GO_ON in
Y | y) echo
	echo "继续配置";;
N | n) echo
	echo "停止配置，退出脚本运行"
	exit;;
esac
function step1(){
	## 在终端输入如下命令安装编译相关的依赖工具
	# 后面的liblz4-tool openjdk-8-jdk genext2fs libssl-dev是补充安装的
	echo "安装依赖组件：binutils git git-lfs gnupg flex bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip m4 bc gnutls-bin python3.8 python3-pip ruby python liblz4-tool openjdk-8-jdk genext2fs libssl-dev libncurses5"
    sudo apt-get update && sudo apt-get install binutils git git-lfs gnupg flex bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip m4 bc gnutls-bin python3.8 python3-pip ruby python liblz4-tool openjdk-8-jdk genext2fs libssl-dev libncurses5
		pip3 install dataclasses
}
function step2(){
    read -p "请输入要配置的用户名(英文名):" GIT_NAME
	read -p "请输入要配置的用户邮箱(英文名):" GIT_EMAIL

	git config --global user.name $GIT_NAME
	git config --global user.email $GIT_EMAIL
	git config --global credential.helper store
	# 生成密钥对
	ssh-keygen -t rsa -b 4096 -C $GIT_EMAIL

	echo "已经配置user.name为$GIT_NAME，配置user.email为$GIT_EMAIL"
	cat ~/.ssh/id_rsa.pub
	echo "请将上面的公钥信息拷贝配置到你的gitee网站中去。"
	read -p "确保已经拷贝配置了公钥:Y/N:" answer
	read -n1 -p "再次确保是否已经拷贝配置了公钥:Y/N:" answer
	case $answer in
	Y | y) echo
		echo "成功配置了公钥，进行步骤3";;
	N | n) echo
		echo "没有拷贝公钥，退出脚本运行"
		exit;;
esac
}

function step3 {
	sudo chmod 777 /usr/local/bin/
	curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > /usr/local/bin/repo
	sudo chmod 777 /usr/local/bin/repo
}

function step4 {
	repo init -u git@gitee.com:openharmony/manifest.git -b master --no-repo-verify
	repo sync -c
	repo forall -c 'git lfs pull'
}

echo "============================================================================"
echo "执行步骤1 安装必要组件"
echo "============================================================================"

read -n1 -p "是否需要安装依赖组件Y/N:" ANSWER_INSTALL_COM

case $ANSWER_INSTALL_COM in
N | n) echo
	echo "继续下一步骤";;
Y | y) echo
	echo "安装组件"
	step1
	;;
esac

echo "============================================================================"
echo "执行步骤2 配置git账户信息"
echo "============================================================================"

read -n1 -p "是否需要配置git环境Y/N:" ANSWER_GIT

case $ANSWER_GIT in
N | n) echo
	echo "继续下一步骤";;
Y | y) echo
	echo "配置git"
    step2
	;;
esac



echo "============================================================================"
echo "执行步骤3 安装码云repo工具"
echo "============================================================================"
read -n1 -p "是否需要安装repo工具Y/N:" ANSWER_REPO

case $ANSWER_REPO in
N | n) echo
	echo "继续下一步骤";;
Y | y) echo
	echo "安装码云repo工具"
    step3
	;;
esac



echo "============================================================================"
echo "执行步骤4 代码下载"
echo "============================================================================"

read -n1 -p "是否需要下载代码Y/N:" ANSWER_CODE

case $ANSWER_CODE in
N | n) echo
	echo "继续下一步骤";;
Y | y) echo
	echo "下载代码"
    step4
	;;
esac


echo "============================================================================"
echo "执行步骤5 代码编译"
echo "============================================================================"
## 执行prebuilts，在源码根目录执行脚本，安装编译器及二进制工具
sudo bash build/prebuilts_download.sh
echo "安装编译器及二进制工具完成"


read -n1 -p "确认是否要进行全量编译:Y/N:" answer
case $answer in
Y | y) echo
	echo "进行全量编译"
	sudo ./build.sh --product-name rk3568 --ccache;;
N | n) echo
	echo "不进行全量编译，退出脚本"
	exit;;
esac
echo "编译完成"