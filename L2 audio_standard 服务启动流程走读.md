## L2 audio_standard 服务启动流程走读

### AudioPolicy服务启动流程

我以涉及到的文件作为代码走读的主轴

#### audio_policy_server.h

services\include\audio_policy\server\audio_policy_server.h

> ```c++
> class AudioPolicyServer : public SystemAbility, public AudioPolicyManagerStub, public AudioSessionCallback 
> ```



#### audio_policy_server.cpp

AudioPolicy服务继承自SystemAbility；当OnStart()被触发时，AudioPolicy服务开始启动

services\src\audio_policy\server\audio_policy_server.cpp

> ```c++
> void AudioPolicyServer::OnStart()
> {
>     //发布服务，客户端可以访问
>     bool res = Publish(this);
>     if (res) {
>         MEDIA_DEBUG_LOG("AudioPolicyService OnStart res=%d", res);
>     }
> 	
>     //服务初始化
>     mPolicyService.Init();
>     
>     RegisterAudioServerDeathRecipient();
>     return;
> }
> ```



#### audio_policy_service.h

展开AudioPolicy初始化的逻辑

services\src\audio_policy\server\service\include\audio_policy_service.h

> ```c++
> //构造函数会初始化一些后面用到的变量
> AudioPolicyService()
>         : mAudioPolicyManager(AudioPolicyManagerFactory::GetAudioPolicyManager()),
>           mConfigParser(ParserFactory::GetInstance().CreateParser(*this))
>     {
>         mDeviceStatusListener = std::make_unique<DeviceStatusListener>(*this);
>     }
> ```



#### audio_policy_service.cpp

进入 mPolicyService.Init();

services\src\audio_policy\server\service\src\audio_policy_service.cpp

> ```c++
> bool AudioPolicyService::Init(void)
> {
>     //大部分初始化逻辑在此
>     mAudioPolicyManager.Init();
>     if (!mConfigParser.LoadConfiguration()) {
>         MEDIA_ERR_LOG("Audio Config Load Configuration failed");
>         return false;
>     }
>     if (!mConfigParser.Parse()) {
>         MEDIA_ERR_LOG("Audio Config Parse failed");
>         return false;
>     }
> 
>     std::unique_ptr<AudioFocusParser> audioFocusParser;
>     audioFocusParser = make_unique<AudioFocusParser>();
>     std::string AUDIO_FOCUS_CONFIG_FILE = "/etc/audio/audio_interrupt_policy_config.xml";
> 
>     if (audioFocusParser->LoadConfig(focusTable_[0][0])) {
>         MEDIA_ERR_LOG("Audio Interrupt Load Configuration failed");
>         return false;
>     }
> 
>     auto samgr = SystemAbilityManagerClient::GetInstance().GetSystemAbilityManager();
>     if (samgr == nullptr) {
>         MEDIA_ERR_LOG("[Policy Service] Get samgr failed");
>         return false;
>     }
> 
>     sptr<IRemoteObject> object = samgr->GetSystemAbility(AUDIO_DISTRIBUTED_SERVICE_ID);
>     if (object == nullptr) {
>         MEDIA_DEBUG_LOG("[Policy Service] audio service remote object is NULL.");
>         return false;
>     }
>     g_sProxy = iface_cast<IStandardAudioService>(object);
>     if (g_sProxy == nullptr) {
>         MEDIA_DEBUG_LOG("[Policy Service] init g_sProxy is NULL.");
>         return false;
>     } else {
>         MEDIA_DEBUG_LOG("[Policy Service] init g_sProxy is assigned.");
>     }
> 
>     if (mDeviceStatusListener->RegisterDeviceStatusListener(nullptr)) {
>         MEDIA_ERR_LOG("[Policy Service] Register for device status events failed");
>         return false;
>     }
> 
>     return true;
> }
> ```



#### audio_adapter_manager.cpp

进入mAudioPolicyManager.Init();  

*tips:我觉得这个变量命名不合理，这里是在连接 pulseaudio，命名为 mAudioAdapterManager更合理*

services\src\audio_policy\server\service\src\manager\audio_adapter_manager.cpp

> ```c++
> bool AudioAdapterManager::Init()
> {
>     std::unique_ptr<AudioAdapterManager> audioAdapterManager(this);
>     std::unique_ptr<PolicyCallbackImpl> policyCallbackImpl = std::make_unique<PolicyCallbackImpl>(audioAdapterManager);
>     mAudioServiceAdapter = AudioServiceAdapter::CreateAudioAdapter(std::move(policyCallbackImpl));
>     //连接 pulseaudio
>     bool result = mAudioServiceAdapter->Connect();
>     if (!result) {
>         MEDIA_ERR_LOG("[AudioAdapterManager] Error in connecting audio adapter");
>         return false;
>     }
>     bool isFirstBoot = false;
>     //初始化音频策略
>     InitAudioPolicyKvStore(isFirstBoot);
>     //初始化音量
>     InitVolumeMap(isFirstBoot);
>     //初始化响铃模式
>     InitRingerMode(isFirstBoot);
> 
>     return true;
> }
> ```



#### pulse_audio_service_adapter_impl.cpp

进入mAudioServiceAdapter->Connect();

frameworks\native\audioadapter\src\pulse_audio_service_adapter_impl.cpp

> ```c++
> bool PulseAudioServiceAdapterImpl::Connect()
> {
>     mMainLoop = pa_threaded_mainloop_new();
>     if (!mMainLoop) {
>         MEDIA_ERR_LOG("[PulseAudioServiceAdapterImpl] MainLoop creation failed");
>         return false;
>     }
> 
>     if (pa_threaded_mainloop_start(mMainLoop) < 0) {
>         MEDIA_ERR_LOG("[PulseAudioServiceAdapterImpl] Failed to start mainloop");
>         pa_threaded_mainloop_free (mMainLoop);
>         return false;
>     }
> 
>     pa_threaded_mainloop_lock(mMainLoop);
> 
>     while (true) {
>         pa_context_state_t state;
> 
>         if (mContext != NULL) {
>             state = pa_context_get_state(mContext);
>             if (state == PA_CONTEXT_READY)
>                 break;
>             // if pulseaudio is ready, retry connect to pulseaudio. before retry wait for sometime. reduce sleep later
>             usleep(PA_CONNECT_RETRY_SLEEP_IN_MICRO_SECONDS);
>         }
> 		//连接pulseaudio
>         bool result = ConnectToPulseAudio();
>         
>         if (!result || !PA_CONTEXT_IS_GOOD(pa_context_get_state(mContext))) {
>             continue;
>         }
> 
>         MEDIA_DEBUG_LOG("[PulseAudioServiceAdapterImpl] pa context not ready... wait");
> 
>         // Wait for the context to be ready
>         pa_threaded_mainloop_wait(mMainLoop);
>     }
> 
>     pa_threaded_mainloop_unlock(mMainLoop);
> 
>     return true;
> }
> ```



进入 ConnectToPulseAudio(); 

frameworks\native\audioadapter\src\pulse_audio_service_adapter_impl.cpp

> ```c++
> bool PulseAudioServiceAdapterImpl::ConnectToPulseAudio()
> {
>     unique_ptr<UserData> userData = make_unique<UserData>();
>     userData->thiz = this;
> 
>     if (mContext != NULL) {
>         pa_context_disconnect(mContext);
>         pa_context_set_state_callback(mContext, NULL, NULL);
>         pa_context_set_subscribe_callback(mContext, NULL, NULL);
>         pa_context_unref(mContext);
>     }
> 
>     pa_proplist *proplist = pa_proplist_new();
>     pa_proplist_sets(proplist, PA_PROP_APPLICATION_NAME, "PulseAudio Service");
>     pa_proplist_sets(proplist, PA_PROP_APPLICATION_ID, "com.ohos.pulseaudio.service");
>     mContext = pa_context_new_with_proplist(pa_threaded_mainloop_get_api(mMainLoop), NULL, proplist);
>     pa_proplist_free(proplist);
> 
>     if (mContext == NULL) {
>         MEDIA_ERR_LOG("[PulseAudioServiceAdapterImpl] creating pa context failed");
>         return false;
>     }
> 
>     pa_context_set_state_callback(mContext,  PulseAudioServiceAdapterImpl::PaContextStateCb, this);
>     //执行pa_context_connect连接pulseaudio，触发 PaContextStateCb 回调
>     if (pa_context_connect(mContext, NULL, PA_CONTEXT_NOFAIL, NULL) < 0) {
>         if (pa_context_errno(mContext) == PA_ERR_INVALID) {
>             MEDIA_ERR_LOG("[PulseAudioServiceAdapterImpl] pa context connect failed: %{public}s",
>                 pa_strerror(pa_context_errno(mContext)));
>             goto Fail;
>         }
>     }
> 
>     return true;
> 
> Fail:
>     /* Make sure we don't get any further callbacks */
>     pa_context_set_state_callback(mContext, NULL, NULL);
>     pa_context_set_subscribe_callback(mContext, NULL, NULL);
>     pa_context_unref(mContext);
>     return false;
> }
> ```



进入PaContextStateCb 回调

frameworks\native\audioadapter\src\pulse_audio_service_adapter_impl.cpp

> ```c++
> void PulseAudioServiceAdapterImpl::PaContextStateCb(pa_context *c, void *userdata)
> {
>     PulseAudioServiceAdapterImpl *thiz = reinterpret_cast<PulseAudioServiceAdapterImpl*>(userdata);
> 
>     switch (pa_context_get_state(c)) {
>         case PA_CONTEXT_UNCONNECTED:
>         case PA_CONTEXT_CONNECTING:
>         case PA_CONTEXT_AUTHORIZING:
>         case PA_CONTEXT_SETTING_NAME:
>             break;
> 		//PA_CONTEXT_READY 表示已经连上pulseaudio
>         case PA_CONTEXT_READY: {
>             pa_context_set_subscribe_callback(c, PulseAudioServiceAdapterImpl::PaSubscribeCb, thiz);
> 			//注册回调用于监听各种事件-由入参掩码决定
>             pa_operation *operation = pa_context_subscribe(c, (pa_subscription_mask_t)
>                 (PA_SUBSCRIPTION_MASK_SINK | PA_SUBSCRIPTION_MASK_SOURCE |
>                 PA_SUBSCRIPTION_MASK_SINK_INPUT | PA_SUBSCRIPTION_MASK_SOURCE_OUTPUT |
>                 PA_SUBSCRIPTION_MASK_CARD), NULL, NULL);
>             if (operation == NULL) {
>                 pa_threaded_mainloop_signal(thiz->mMainLoop, 0);
>                 return;
>             }
>             pa_operation_unref(operation);
>             pa_threaded_mainloop_signal(thiz->mMainLoop, 0);
>             break;
>         }
> 
>         case PA_CONTEXT_FAILED:
>             pa_threaded_mainloop_signal(thiz->mMainLoop, 0);
>             return;
> 
>         case PA_CONTEXT_TERMINATED:
>         default:
>             return;
>     }
> }
> ```



#### 总结

至此，AudioPolicy服务完成了初始化流程，其中最重要的是

- 连接pulseaudio
- 初始化各类参数
- 发布服务使客户端可以调用



### PulseAudio连接流程

1. Create a PulseAudio main loop (synchronous: **pa_mainloop_new**). 
2. Get the mainloop API object, which is a table of mainloop functions  (synchronous: **pa_mainloop_get_api**). 
3. Get a context object to talk to the PulseAudio server (synchronous: **pa_context_ new**). 
4. Establish a connection to the PulseAudio server. This is asynchronous: **pa_ context_connect**. 
5. Register a callback for context state changes from the server: **pa_context_set_ state_callback**. 
6. Commence the event-processing loop (**pa_mainloop_run**). 

上述一套流程下来就可以成功连接上pulseaudio，下面是简化的连接pulseaudio的流程

```c++
pa_mainloop *pa_ml;
    pa_mainloop_api *pa_mlapi;
    pa_operation *pa_op;
    pa_time_event *time_event;
    //创建mainloop
    pa_ml = pa_mainloop_new();
    //创建api
    pa_mlapi = pa_mainloop_get_api(pa_ml);
    //创建context
    context = pa_context_new(pa_mlapi, "test");
    //连接
    pa_context_connect(context, NULL, 0, NULL);
    //注册状态监听回调
    pa_context_set_state_callback(context, state_cb, NULL);
    //开启事件循环(未开启时，不会处理连接事件)
    if (pa_mainloop_run(pa_ml, &ret) < 0) {
        printf("pa_mainloop_run() failed.");
        exit(1);
    }
```



### AudioServer启动流程

我以涉及到的文件作为代码走读的主轴

#### audio_server.h

AudioServer的定义，找模块内的系统服务找SystemAbility即可，系统服务会继承自SystemAbility

services\include\server\audio_server.h

```c++
class AudioServer : public SystemAbility, public AudioManagerStub
```



#### audio_server.cpp

展开 onStart()

services\src\server\audio_server.cpp

```c++
void AudioServer::OnStart()
{
    MEDIA_DEBUG_LOG("AudioService OnStart");
    bool res = Publish(this);
    if (res) {
        MEDIA_DEBUG_LOG("AudioService OnStart res=%{public}d", res);
    }

#ifdef PA
    //启动运行pulseaudio的线程 paDaemonThread
    int32_t ret = pthread_create(&m_paDaemonThread, nullptr, AudioServer::paDaemonThread, nullptr);
    if (ret != 0) {
        MEDIA_ERR_LOG("pthread_create failed %d", ret);
    }
    MEDIA_INFO_LOG("Created paDaemonThread\n");
#endif
}
```



展开 paDaemonThread

services\src\server\audio_server.cpp

```c++
void *AudioServer::paDaemonThread(void *arg)
{
    /* Load the mandatory pulseaudio modules at start */
    char *argv[] = {
        (char*)"pulseaudio",
    };

    MEDIA_INFO_LOG("Calling ohos_pa_main\n");
    //启动 pulseaudio
    ohos_pa_main(PA_ARG_COUNT, argv);
    MEDIA_INFO_LOG("Exiting ohos_pa_main\n");
    exit(-1);
}
```



ohos_pa_main这个函数在pulseaudio的仓下面，不展开讲解

pulseaudio\src\daemon\ohos_pa_main.c



总结：

AudioServer和AudioPolicy区别在于调用接口的区别；AudioServer会调用HDI接口；AudioPolicy会调用pulseaudio的接口，不过启动pulseaudio本身是AudioServer来做的

AudioServer的功能主要有以下两种

- 启动pulseaudio
- 封装HDI接口，为客户端提供服务接口

*Tips：我认为AudioServer的名字和它的功能是不相称，后续考虑使用更合理的名字*